﻿﻿using MediatR;

namespace Primitives;

public abstract class DomainEvent:INotification
{
    public Guid Id { get; protected set; }
}