﻿using CSharpFunctionalExtensions;

namespace Primitives;

public abstract class Aggregate: Entity<Guid>, IAggregateRoot
{
    private readonly List<DomainEvent> _domainEvents = new();
    
    public IReadOnlyList<DomainEvent> DomainEvents => _domainEvents;

        protected Aggregate(Guid id) : base(id)
    {
        
    }

    protected Aggregate()
    {
        
    }

    
    protected void RaiseDomainEvent(DomainEvent domainEvent)
    {
        _domainEvents.Add(domainEvent);
    }
    
    public IReadOnlyCollection<DomainEvent> GetDomainEvents() => _domainEvents.ToList();
    
    public void ClearDomainEvents() => _domainEvents.Clear();
    }
    
public interface IAggregateRoot
{
        
}