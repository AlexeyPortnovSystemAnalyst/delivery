﻿# OpenApi
```
openapi-generator generate -i https://gitlab.com/microarch-ru/microservices/dotnet/system-design/-/raw/main/services/delivery/contracts/openapi.yml -g aspnetcore -o ./Contract --package-name Api --additional-properties classModifier=abstract --additional-properties operationResultTask=true
openapi-generator-cli generate -g aspnetcore --additional-properties aspnetCoreVersion=3.1 --additional-properties classModifier=abstract --additional-properties operationResultTask=true --additional-properties buildTarget=library --additional-properties  operationModifier=abstract --additional-properties packageName=Api --additional-properties packageTitle=Api -i https://gitlab.com/microarch-ru/microservices/dotnet/system-design/-/raw/main/services/delivery/contracts/openapi.yml
```
# БД 
```
dotnet tool install --global dotnet-ef
dotnet tool update --global dotnet-ef
dotnet add package Microsoft.EntityFrameworkCore.Design
```
[Подробнее про dotnet cli](https://learn.microsoft.com/ru-ru/ef/core/cli/dotnet)

# Миграции
```
cd /home/msa/gitlab.com/delivery/DeliveryApp.Infrastructure
dotnet ef migrations add Init --startup-project ../DeliveryApp.Ui/DeliveryApp.Ui.csproj --project ./DeliveryApp.Infrastructure.csproj
dotnet ef database update --startup-project ../DeliveryApp.Ui/DeliveryApp.Ui.csproj --project ./DeliveryApp.Infrastructure.csproj

dotnet ef database update --startup-project ../DeliveryApp.Ui/DeliveryApp.Ui.csproj --connection "Server=localhost;Port=5432;User Id=username;Password=secret;Database=delivery;"

dotnet new tool-manifest # если файл манифеста инструмента еще не создан
dotnet tool install --local dotnet-ef --version 8.0.2

migrationBuilder.InsertData(
    table: "transports",
    columns: new[] { "id", "name", "speed", "capacity" },
    values: new object[,]
    {
        { 1, "Pedestrian", 1, 10 }, // Пример значения capacity
        { 2, "Bicycle", 2, 15 },    // Пример значения capacity
        { 3, "Scooter", 3, 20 },    // Пример значения capacity
        { 4, "Car", 4, 30 }         // Пример значения capacity
    });
    


cd 




dotnet ef migrations add Init --startup-project ./DeliveryApp.Ui --project ./DeliveryApp.Infrastructure

dotnet ef migrations add InitialCreate --startup-project ./DeliveryApp.Ui/DeliveryApp.Ui.csproj --project ./DeliveryApp.Infrastructure.csproj



dotnet ef migrations add InitialCreate --startup-project ../DeliveryApp.Ui/DeliveryApp.Ui.csproj


dotnet ef migrations add InitialCreate  --startup-project ..\DeliveryApp.Ui
dotnet ef migrations add Init --startup-project ./DeliveryApp.Api --project ./DeliveryApp.Infrastructure


dotnet ef database update --startup-project ./DeliveryApp.Api --connection "Server=localhost;Port=5432;User Id=username;Password=secret;Database=delivery;"
```
# Docker
```
docker rm -vf $(docker ps -aq)
docker volume rm $(docker volume ls -q)
docker rmi -f $(docker images -aq)
```

# Docker Compose
```
docker-compose down --remove-orphans
docker-compose up
docker-compose up postgres
```
