using Contracts.RabbitMq;
using DeliveryApp.Core.Application.UseCases.Commands.CreateOrder;
using MassTransit;
using MediatR;

namespace DeliveryApp.Ui.Adapters.RabbitMq
{
    public class BasketConfirmedConsumer : IConsumer<BasketConfirmedIntegrationEvent>
    {
        private readonly IMediator _mediator;

        public BasketConfirmedConsumer(IMediator mediator)
        {
            _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
        }
        public async Task Consume(ConsumeContext<BasketConfirmedIntegrationEvent> context)
        {
            var basketId = context.Message.BasketId;
            var street = context.Message.Street;
            var weight = context.Message.Weight;

            var basketConfirmedIntegrationEventsCommand = new Command(basketId, street, weight);

            var result  = await _mediator.Send(basketConfirmedIntegrationEventsCommand);

            if (result)
            {
                Console.WriteLine("Order creation succeeded!");
            }
            else
            {
                Console.WriteLine("Order creation failed!");
            }
        }
    }
}