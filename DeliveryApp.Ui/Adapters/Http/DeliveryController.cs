using Api.Controllers;
using Api.Models;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace DeliveryApp.Ui.Adapters.Http;

public class DeliveryController : DefaultApiController
{
    private readonly IMediator _mediator;

    public DeliveryController(IMediator mediator)
    {
        _mediator = mediator ?? throw new ArgumentNullException(nameof(mediator));
    }

            // метод для вызова заполнения 
        [HttpGet("/fill")]
        public async Task<IActionResult> PopulateRepositoriesDB()
        {
        Console.WriteLine("Привет от сервера! FIll");
        var fillCourierDB = new Core.Application.UseCases.Commands.PopulateRepositories.Command();
        var response = await _mediator.Send(fillCourierDB);
        if(response != null) return Ok(response); // Возвращаем список имен курьеров
        return Conflict();
        }

        public override async Task<IActionResult> AssignOrder()
        {
            var assignOrderToCourierCommand = new Core.Application.UseCases.Commands.AssignOrderToCourier.Command();
            var response = await _mediator.Send(assignOrderToCourierCommand);
            if(response) return Ok();
            return Conflict();
        }

        public override async Task<IActionResult> CreateOrder()
        {

            var streets = new[]
        {
            "Тестировочная", "Айтишная", "Эйчарная", 
            "Аналитическая", "Нагрузочная", "Серверная", 
            "Мобильная", "Бажная"
        };

  
        var random = new Random();
        var randomStreet = streets[random.Next(streets.Length)];
        var randomWeight = random.Next(1, 12);



            var createOrderCommand = new Core.Application.UseCases.Commands.CreateOrder.Command(
                Guid.NewGuid(),randomStreet, randomWeight);
            var response = await _mediator.Send(createOrderCommand);
            if(response) return Created();
            return Conflict();
        }

        public override async Task<IActionResult> GetOrder(Guid orderId)
        {
            var getOrderQuery = new Core.Application.UseCases.Queries.GetOrder.Query(orderId);
            var response = await _mediator.Send(getOrderQuery);
            return Ok(response);
        }

        public override async Task<IActionResult> Move()
        {
            var moveToOrderCommand = new Core.Application.UseCases.Commands.MoveToOrder.Command();
            var response = await _mediator.Send(moveToOrderCommand);
            if(response) return Ok();
            return Conflict();
        }

        public override async Task<IActionResult> StartWork(Guid courierId)
        {
            var startWorkCommand = new Core.Application.UseCases.Commands.StartWork.Command(courierId);
            var response = await _mediator.Send(startWorkCommand);
            if(response) return Ok();
            return Conflict();
        }

        public override async Task<IActionResult> StopWork(Guid courierId)
        {
            var stopWorkCommand = new Core.Application.UseCases.Commands.StopWork.Command(courierId);
            var response = await _mediator.Send(stopWorkCommand);
            if(response) return Ok();
            return Conflict();
        }



    public class CreateOrderModel
    {
        public Guid BasketId { get; set; }
        public string Street { get; set; }
        public int Weight { get; set; }
    }
}
