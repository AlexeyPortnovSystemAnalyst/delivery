using DeliveryApp.Core.Domain.SharedKernel;
using DeliveryApp.Core.Ports;
using GeoApp.Ui; // Импортируйте сгенерированное пространство имен gRPC
using Grpc.Core;
using Grpc.Net.Client;
using Grpc.Net.Client.Configuration;

namespace DeliveryApp.Infrastructure.Adapters.Grpc.GeoService;

public class GeoClient : IGeoClient
{
    private readonly string _url;
    private readonly SocketsHttpHandler _socketsHttpHandler;
    private readonly MethodConfig _methodConfig;

    public GeoClient(string url)
    {
        _url = url;

        _socketsHttpHandler = new SocketsHttpHandler
        {
            PooledConnectionIdleTimeout = Timeout.InfiniteTimeSpan,
            KeepAlivePingDelay = TimeSpan.FromSeconds(60),
            KeepAlivePingTimeout = TimeSpan.FromSeconds(30),
            EnableMultipleHttp2Connections = true
        };

        _methodConfig = new MethodConfig
        {
            Names = { MethodName.Default },
            RetryPolicy = new RetryPolicy
            {
                MaxAttempts = 5,
                InitialBackoff = TimeSpan.FromSeconds(1),
                MaxBackoff = TimeSpan.FromSeconds(5),
                BackoffMultiplier = 1.5,
                RetryableStatusCodes = { StatusCode.Unavailable }
            }
        };
    }

public async Task<LocationDto> GetGeolocationAsync(string address)
        {
            using var channel = GrpcChannel.ForAddress(_url, new GrpcChannelOptions
            {
                HttpHandler = _socketsHttpHandler,
                ServiceConfig = new ServiceConfig { MethodConfigs = { _methodConfig } }
            });

            var client = new Geo.GeoClient(channel);
            try
            {
                var reply = await client.GetGeolocationAsync(new GetGeolocationRequest
                {
                    Address = address
                });
                
                // Возвращаем DTO
                return new LocationDto
                {
                    X = reply.Location.X,
                    Y = reply.Location.Y
                };
            }
            catch (RpcException)
            {
                // Обработка ошибки
                return null; // Рекомендуется использовать логику по умолчанию или выбрасывать исключение
            }
        }

    }
