using DeliveryApp.Core.Ports;
using Microsoft.EntityFrameworkCore.Storage;
using Primitives;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace DeliveryApp.Infrastructure.Adapters.inMemory
{
    public class InMemoryUnitOfWork : IUnitOfWork
    {
        public Task<bool> SaveEntitiesAsync(CancellationToken cancellationToken = default)
        {
            // Просто подтверждаем, что все изменения "сохранены".
            return Task.FromResult(true);
        }

        public Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            // Подтверждаем, что "изменения" сохранены, возвращаем 0, как индикатор того, что изменений нет.
            return Task.FromResult(0);
        }

        public Task<IDbContextTransaction> BeginTransactionAsync()
        {
            // Возвращаем null или имитацию транзакции, так как в контексте InMemory нет необходимости в транзакциях.
            // Важно обеспечить соответствие типа возвращаемого значения интерфейсу.
            return Task.FromResult<IDbContextTransaction>(null);
        }

        public Task CommitTransactionAsync(IDbContextTransaction transaction)
        {
            // Просто подтверждаем "сохранение" транзакции, ничего не делаем.
            return Task.CompletedTask;
        }

        public void Dispose()
        {
            // Ничего не делаем при уничтожении, так как это временная имитация.
        }
    }
}
