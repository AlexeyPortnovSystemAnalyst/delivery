﻿using System.Text.Json;
using DeliveryApp.Core.Domain.CourierAggregate;
using DeliveryApp.Core.Ports;
using Primitives;


namespace DeliveryApp.Infrastructure.Adapters.inMemory.InMemoryCourierRepository
{
    /// <summary>
    /// Временная реализация репозитория курьеров, хранящая данные в памяти
    /// </summary>
    public class InMemoryCourierRepository : ICourierRepository
    {
        private readonly Dictionary<Guid, Courier> _couriers = new();

                private readonly IUnitOfWork _unitOfWork = new InMemoryUnitOfWork();

        public IUnitOfWork UnitOfWork => _unitOfWork;



        public Courier Add(Courier courier)
        {
            if (courier == null) throw new ArgumentNullException(nameof(courier));
            _couriers[courier.Id] = courier;
            Console.WriteLine("REPO: save courier -  ", courier.Id);
            return courier;
        }

        public void Update(Courier courier)
        {
            if (courier == null) throw new ArgumentNullException(nameof(courier));
            if (!_couriers.ContainsKey(courier.Id))
            {
                throw new KeyNotFoundException($"Курьер с идентификатором {courier.Id} не найден.");
            }
            _couriers[courier.Id] = courier;
        }

        public async Task<Courier> GetAsync(Guid courierId)
        {
            if (_couriers.TryGetValue(courierId, out var courier))
            {
                return await Task.FromResult(courier);
            }
            return null;
        }

    public IEnumerable<Courier> GetAllActive()
    {
        // var activeCouriers = _couriers.Values.Where(c => c.Status == CourierStatus.Ready).ToList();
        var activeCouriers = _couriers.Values.ToList();
        Console.WriteLine("Привет от repository! Couriers :");
        foreach (var courier in activeCouriers)
        {
            Console.WriteLine($"{courier.Name} - {courier.Status}");
        }
        return activeCouriers;
    }

    }
}
