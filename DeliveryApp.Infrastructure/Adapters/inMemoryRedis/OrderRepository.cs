﻿using DeliveryApp.Core.Domain.OrderAggregate;
using DeliveryApp.Core.Ports;
using Primitives;
using System.Text.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliveryApp.Infrastructure.Adapters.inMemory.InMemoryOrderRepository
{
    public class InMemoryOrderRepository : IOrderRepository
    {
        private readonly List<Order> _orders = new List<Order>();

        private readonly IUnitOfWork _unitOfWork = new InMemoryUnitOfWork();

        public IUnitOfWork UnitOfWork => _unitOfWork;

        public Order Add(Order order)
        {
            if (order == null) throw new ArgumentNullException(nameof(order));
            _orders.Add(order);
            return order;
        }

        public void Update(Order order)
        {
            // Для простоты, обновление во временной реализации не делает ничего,
            // так как мы работаем с ссылочными типами и изменения в order будут видны в _orders.
            // В реальном приложении здесь должна быть логика для обновления сущности.
        }

        public async Task<Order> GetAsync(Guid orderId)
        {
            // Используем Task.FromResult для имитации асинхронности.
            return await Task.FromResult(_orders.FirstOrDefault(o => o.Id == orderId));
        }

        public IEnumerable<Order> GetAllActive()
        {
         var json = JsonSerializer.Serialize(_orders.Where(o => o.Status == OrderStatus.Created), new JsonSerializerOptions { WriteIndented = true });
         Console.WriteLine($"Привет от repository! Orders:\n{json}");
            // return _orders.Where(o => o.Status == OrderStatus.Created);
            return _orders;
            
        }
    }
}
