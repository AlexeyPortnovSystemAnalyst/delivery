using DeliveryApp.Infrastructure.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DeliveryApp.Infrastructure.EntityConfigurations.Outbox;

class OutboxEntityTypeConfiguration : IEntityTypeConfiguration<OutboxMessage>
{
    public void Configure(EntityTypeBuilder<OutboxMessage> orderConfiguration)
    {
        orderConfiguration
            .ToTable("outbox");

        orderConfiguration
            .Property(entity => entity.Id)
            .ValueGeneratedNever()
            .HasColumnName("id");

        orderConfiguration
            .Property(entity=>entity.Type)
            .UsePropertyAccessMode(PropertyAccessMode.Field)
            .HasColumnName("type")
            .IsRequired();

        orderConfiguration
            .Property(entity => entity.Content)
            .UsePropertyAccessMode(PropertyAccessMode.Field)
            .HasColumnName("content")
            .IsRequired();
        
        orderConfiguration
            .Property(entity => entity.OccuredOnUtc)
            .UsePropertyAccessMode(PropertyAccessMode.Field)
            .HasColumnName("occuredOnUtc")
            .IsRequired();
        
        orderConfiguration
            .Property(entity => entity.ProcessedOnUtc)
            .UsePropertyAccessMode(PropertyAccessMode.Field)
            .HasColumnName("ProcessedOnUtc")
            .IsRequired(false);
        
        orderConfiguration
            .Property(entity => entity.Error)
            .UsePropertyAccessMode(PropertyAccessMode.Field)
            .HasColumnName("error")
            .IsRequired(false);
    }
}