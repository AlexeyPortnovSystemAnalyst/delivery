﻿using DeliveryApp.Core.Domain.CourierAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DeliveryApp.Infrastructure.EntityConfigurations.CourierAggregate
{
    class CourierStatusEntityTypeConfiguration : IEntityTypeConfiguration<CourierStatus>
    {
        public void Configure(EntityTypeBuilder<CourierStatus> entityTypeBuilder)
        {
            entityTypeBuilder.ToTable("courier_statuses");

            entityTypeBuilder.HasKey(o => o.Id);
                
            entityTypeBuilder
                .Property(c => c.Id)
                .ValueGeneratedNever()
                .HasColumnName("id")
                .IsRequired();
        
            entityTypeBuilder
                .Property(c => c.Name)
                .HasColumnName("name")
                .IsRequired();
        }
    }
}