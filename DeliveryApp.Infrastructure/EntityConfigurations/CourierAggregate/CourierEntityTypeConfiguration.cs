﻿using DeliveryApp.Core.Domain.CourierAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DeliveryApp.Infrastructure.EntityConfigurations.CourierAggregate
{
    public class CourierEntityTypeConfiguration : IEntityTypeConfiguration<Courier>
    {
        public void Configure(EntityTypeBuilder<Courier> entityTypeBuilder)
        {
            entityTypeBuilder.ToTable("couriers");
        
            // entityTypeBuilder.Ignore(c => c.DomainEvents);

            entityTypeBuilder.HasKey(c => c.Id);
        
            entityTypeBuilder
                .Property(c => c.Id)
                .ValueGeneratedNever()
                .HasColumnName("id")
                .IsRequired();
        
            entityTypeBuilder
                .Property(c => c.Name)
                .HasColumnName("name")
                .IsRequired();
        
            entityTypeBuilder
                .HasOne(c => c.Status)
                .WithMany()
                .HasForeignKey("status_id")
                .IsRequired();
            
            entityTypeBuilder
                .HasOne(c => c.Transport)
                .WithMany()
                .HasForeignKey("transport_id")
                .IsRequired();
            
            entityTypeBuilder
                .OwnsOne(c => c.Location, l =>
                {
                    l.Property(loc => loc.X).HasColumnName("location_x").IsRequired();
                    l.Property(loc => loc.Y).HasColumnName("location_y").IsRequired();
                    l.WithOwner();
                });
        }
    }
}
