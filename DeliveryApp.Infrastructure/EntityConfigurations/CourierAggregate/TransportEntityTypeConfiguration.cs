﻿using DeliveryApp.Core.Domain.CourierAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DeliveryApp.Infrastructure.EntityConfigurations.CourierAggregate
{
    class TransportEntityTypeConfiguration : IEntityTypeConfiguration<Transport>
    {
        public void Configure(EntityTypeBuilder<Transport> entityTypeBuilder)
        {
            entityTypeBuilder.ToTable("transports");

            entityTypeBuilder.HasKey(o => o.Id);
        
            entityTypeBuilder
                .Property(c => c.Id)
                .ValueGeneratedNever()
                .HasColumnName("id")
                .IsRequired();
        
            entityTypeBuilder
                .Property(c => c.Name)
                .HasColumnName("name")
                .IsRequired();
            
            entityTypeBuilder
                .Property(c => c.Speed)
                .HasColumnName("speed")
                .IsRequired();
            
            entityTypeBuilder
                .OwnsOne(o => o.Capacity, a =>
                {
                    a.Property(c => c.Value).HasColumnName("capacity").IsRequired();
                    a.WithOwner();
                });
        }
    }
}