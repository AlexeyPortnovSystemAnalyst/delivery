﻿using DeliveryApp.Core.Domain.OrderAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace DeliveryApp.Infrastructure.EntityConfigurations.OrderAggregate
{
    class OrderStatusEntityTypeConfiguration : IEntityTypeConfiguration<OrderStatus>
    {
        public void Configure(EntityTypeBuilder<OrderStatus> entityTypeBuilder)
        {
            entityTypeBuilder.ToTable("order_statuses");

            entityTypeBuilder.HasKey(o => o.Id);
        
            entityTypeBuilder
                .Property(c => c.Id)
                .ValueGeneratedNever()
                .HasColumnName("id")
                .IsRequired();
        
            entityTypeBuilder
                .Property(c => c.Name)
                .HasColumnName("name")
                .IsRequired();
        }
    }
}