﻿using DeliveryApp.Core.Domain.OrderAggregate;
using DeliveryApp.Core.Domain.CourierAggregate;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;


namespace DeliveryApp.Infrastructure.EntityConfigurations.OrderAggregate
{
    class OrderEntityTypeConfiguration : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> entityTypeBuilder)
        {
            entityTypeBuilder.ToTable("orders");
        
            // entityTypeBuilder.Ignore(b => b.DomainEvents);

            entityTypeBuilder.HasKey(o => o.Id);
        
            entityTypeBuilder
                .Property(c => c.Id)
                .ValueGeneratedNever()
                .HasColumnName("id")
                .IsRequired();

            entityTypeBuilder.HasOne<Courier>()
            .WithMany()
            .IsRequired(false)
            .HasForeignKey(entity => entity.CourierId)
            .HasPrincipalKey(courier => courier.Id);
        
            entityTypeBuilder
                .Property(c=>c.CourierId)
                .HasColumnName("courier_id")
                .IsRequired(false);
        
            entityTypeBuilder
                .HasOne(o => o.Status)
                .WithMany()
                .IsRequired()
                .HasForeignKey("status_id");
            
            entityTypeBuilder
                .OwnsOne(o => o.Location, a =>
                {
                    a.Property(c => c.X).HasColumnName("location_x").IsRequired();
                    a.Property(c => c.Y).HasColumnName("location_y").IsRequired();
                    a.WithOwner();
                });
            
            entityTypeBuilder
                .OwnsOne(o => o.Weight, a =>
                {
                    a.Property(c => c.Value).HasColumnName("weight").IsRequired();
                    a.WithOwner();
                });
        }
    }
}