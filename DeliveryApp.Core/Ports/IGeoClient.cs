namespace DeliveryApp.Core.Ports
{
    public class LocationDto
    {
        public int X { get; set; }
        public int Y { get; set; }
    }

    public interface IGeoClient
    {
        Task<LocationDto> GetGeolocationAsync(string address);
    }
}

