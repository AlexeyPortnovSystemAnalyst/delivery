﻿using DeliveryApp.Core.DomainServices;
using DeliveryApp.Core.Ports;
using MediatR;

namespace DeliveryApp.Core.Application.UseCases.Commands.AssignOrderToCourier
{
    public class Handler : IRequestHandler<Command, bool>
    {
        private readonly IOrderRepository _orderRepository;
        private readonly ICourierRepository _courierRepository;

        /// <summary>
        /// Ctr
        /// </summary>
        public Handler(IOrderRepository orderRepository, ICourierRepository courierRepository)
        {
            _orderRepository = orderRepository ?? throw new ArgumentNullException(nameof(orderRepository));
            _courierRepository = courierRepository ?? throw new ArgumentNullException(nameof(courierRepository));
        }

        public async Task<bool> Handle(Command message, CancellationToken cancellationToken)
        {
            Console.WriteLine("Assign handler enter");

            // Восстанавливаем аггрегаты
            // var order = _orderRepository.GetAllActive().FirstOrDefault(); 
            var order = _orderRepository.GetAllActive().FirstOrDefault(); 
            if (order == null) {
                Console.WriteLine("*** !!! НЕ получили заказ из репозитория");
                return false;}
            Console.WriteLine("получили заказ из репозитория");

            var couriers = _courierRepository.GetAllActive().ToList();
            if (!couriers.Any()) {
                Console.WriteLine("*** !!! Не получили курьеров из репозитория");
                return false;};

            Console.WriteLine("получили курьеров из репозитория");

            // Распределяем заказы на курьеров
            var dispatchResult = DispatchService.Dispatch(order, couriers);
            if (dispatchResult.IsFailure) return false;
            var courier = dispatchResult.Value;
            Console.WriteLine("Привет от сервера! courier" + courier);

            // Сохраняем назначение одной транзакцией
            await using var transaction = await _orderRepository.UnitOfWork.BeginTransactionAsync();
            _courierRepository.Update(courier);
            _orderRepository.Update(order);
            await _orderRepository.UnitOfWork.CommitTransactionAsync(transaction);

            return true;
        }
    }
}