﻿﻿using System.Runtime.InteropServices;
using DeliveryApp.Core.Ports;
using MediatR;

namespace DeliveryApp.Core.Application.UseCases.Commands.StartWork
{
    public class Handler : IRequestHandler<Command, bool>
    {
        private readonly ICourierRepository _courierRepository;

        /// <summary>
        /// Ctr
        /// </summary>
        public Handler(ICourierRepository courierRepository)
        {
            _courierRepository = courierRepository ?? throw new ArgumentNullException(nameof(courierRepository));
        }

        public async Task<bool> Handle(Command message, CancellationToken cancellationToken)
        {
            //Восстанавливаем аггрегат
            var courier = await _courierRepository.GetAsync(message.CourierId);
            if (courier == null) { Console.WriteLine("StartWork Use case. Courier not found");
                return false;};
            
            //Изменяем аггрегат
            var courierStartWorkResult = courier.StartDay();
            if (courierStartWorkResult.IsFailure) { Console.WriteLine("StartWork Use case. Start day error");
                return false;};
            

            //Сохраняем аггрегат
            _courierRepository.Update(courier);
            Console.WriteLine("StartWork Use case. Couruier was updated" + courier);


            return await _courierRepository.UnitOfWork.SaveEntitiesAsync(cancellationToken);
        }
    }
}