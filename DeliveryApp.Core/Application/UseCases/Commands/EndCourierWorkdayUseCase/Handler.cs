﻿using DeliveryApp.Core.Ports;
using MediatR;

namespace DeliveryApp.Core.Application.UseCases.Commands.StopWork
{
    public class Handler : IRequestHandler<Command, bool>
    {
        private readonly IOrderRepository _orderRepository;

        /// <summary>
        /// Ctr
        /// </summary>
        public Handler(IOrderRepository orderRepository)
        {
            _orderRepository = orderRepository ?? throw new ArgumentNullException(nameof(orderRepository));
        }

        public async Task<bool> Handle(Command message, CancellationToken cancellationToken)
        {
            //Восстанавливаем аггрегат
            var order = await _orderRepository.GetAsync(message.OrderId);
            if (order == null) return false;
            
            //Изменяем аггрегат
            var orderCancelResult = order.Complete();
            if (orderCancelResult.IsFailure) return false;

            //Сохраняем аггрегат
            _orderRepository.Update(order);
            return await _orderRepository.UnitOfWork.SaveEntitiesAsync(cancellationToken);
        }
    }
}