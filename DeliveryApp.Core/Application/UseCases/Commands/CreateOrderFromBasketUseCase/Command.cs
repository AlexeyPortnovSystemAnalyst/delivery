﻿﻿using MediatR;
using Primitives;

namespace DeliveryApp.Core.Application.UseCases.Commands.CreateOrder
{
    /// <summary>
    /// Создать заказ
    /// </summary>
    public class Command : IRequest<bool>
    {
        /// <summary>
        /// Идентификатор корзины
        /// </summary>
        public Guid BasketId { get; init; }

        /// <summary>
        ///     Улица
        /// </summary>
        public string Street { get; init; }

        /// <summary>
        ///     Дом
        /// </summary>
        public int Weight { get; init; }

        /// <summary>
        /// Ctr
        /// </summary>
        /// <param name="basketId">Идентификатор корзины</param> 
        /// <param name="street">Улица</param>
        /// <param name="weight">Вес</param>
        public Command(Guid basketId, string street, int weight)
        {
            //Валидация
            if (basketId == Guid.Empty) GeneralErrors.ValueIsRequired(nameof(basketId));
            if (string.IsNullOrWhiteSpace(street)) GeneralErrors.ValueIsRequired(nameof(street));
            if (weight <= 0) GeneralErrors.ValueIsRequired(nameof(weight));

            //Присваивание
            BasketId = basketId;
            Street = street;
            Weight = weight;
        }
    }
}