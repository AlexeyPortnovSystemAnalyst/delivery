﻿﻿using DeliveryApp.Core.Domain.OrderAggregate;
using DeliveryApp.Core.Domain.SharedKernel;
using DeliveryApp.Core.Ports;
using MediatR;

namespace DeliveryApp.Core.Application.UseCases.Commands.CreateOrder
{
    public class Handler : IRequestHandler<Command, bool>
    {
        private readonly IOrderRepository _orderRepository;
        private readonly IGeoClient _geoClient; // Добавляем клиент для геосервиса

        public Handler(IOrderRepository orderRepository, IGeoClient geoClient)
        {
            _orderRepository = orderRepository ?? throw new ArgumentNullException(nameof(orderRepository));
             _geoClient = geoClient;
        }

        public async Task<bool> Handle(Command message, CancellationToken cancellationToken)
        {
            Console.WriteLine($"Получение геолокации для адреса: {message.Street}");

            var locationDto = await _geoClient.GetGeolocationAsync(message.Street);
            if (locationDto == null)
            {
                Console.WriteLine($"Не удалось получить геолокацию для адреса: {message.Street}");
                return false;
            }
            
            Console.WriteLine($"Геолокация получена для адреса {message.Street}: X={locationDto.X}, Y={locationDto.Y}");
   
            var location = Location.Create(locationDto.X, locationDto.Y);
            

            var weightCreateResult = Weight.Create(message.Weight);
                if (weightCreateResult.IsFailure)
                {
                    Console.WriteLine($"Ошибка при создании веса заказа: {weightCreateResult.Error}");
                    return false;
                }

            var orderCreateResult = Order.Create(message.BasketId,Location.MinLocation,weightCreateResult.Value);
                if (orderCreateResult.IsFailure)
                {
                    Console.WriteLine($"Ошибка при создании заказа: {orderCreateResult.Error}");
                    return false;
                }
            var order = orderCreateResult.Value;
            
            // Сохраняем аггрегат
            _orderRepository.Add(order);
            var result = await _orderRepository.UnitOfWork.SaveEntitiesAsync(cancellationToken);
            
            Console.WriteLine($"Заказ сохранен: {result}");
            return result;
                }
    }
}