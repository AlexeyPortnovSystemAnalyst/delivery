﻿ using MediatR;
 using DeliveryApp.Core.Domain.OrderAggregate;
using DeliveryApp.Core.Domain.CourierAggregate;

namespace DeliveryApp.Core.Application.UseCases.Commands.PopulateRepositories
{

    public class Command : IRequest<List<string>>
    {


        public Command()
        {

        }
    }
}