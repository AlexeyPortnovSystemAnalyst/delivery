﻿﻿using MediatR;
using DeliveryApp.Core.Ports;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using DeliveryApp.Core.Domain.CourierAggregate;
using DeliveryApp.Core.Domain.SharedKernel;

namespace DeliveryApp.Core.Application.UseCases.Commands.PopulateRepositories

{
    public class PopulateRepositoriesHandler : IRequestHandler<Command, List<string>>
    {
        private readonly IOrderRepository _orderRepository;
        private readonly ICourierRepository _courierRepository;

        public PopulateRepositoriesHandler(IOrderRepository orderRepository, ICourierRepository courierRepository)
        {
            _orderRepository = orderRepository ?? throw new ArgumentNullException(nameof(orderRepository));
            _courierRepository = courierRepository ?? throw new ArgumentNullException(nameof(courierRepository));
        }

        public Task<List<string>> Handle(Command command, CancellationToken cancellationToken)
{

            var couriersNames = new List<string>();
            // Создание курьеров с использованием фабричных методов
            var couriersData = new List<(Guid Id, string Name, Transport Transport, int X, int Y)>
            {
                (Guid.Parse("bf79a004-56d7-4e5f-a21c-0a9e5e08d10d"), "Петя", Transport.Pedestrian, 1, 3),
                (Guid.Parse("a9f7e4aa-becc-40ff-b691-f063c5d04015"), "Оля", Transport.Pedestrian, 3, 2),
                (Guid.Parse("db18375d-59a7-49d1-bd96-a1738adcee93"), "Ваня", Transport.Bicycle, 4, 5),
                (Guid.Parse("e7c84de4-3261-476a-9481-fb6be211de75"), "Маша", Transport.Bicycle, 1, 8),
                (Guid.Parse("407f68be-5adf-4e72-81bc-b1d8e9574cf8"), "Игорь", Transport.Scooter, 7, 9),
                (Guid.Parse("006e6c66-087e-4a27-aa59-3c0a2bc945c5"), "Даша", Transport.Scooter, 5, 5),
                (Guid.Parse("40d50b82-ce79-4cde-8ce1-21883f466038"), "Сережа", Transport.Car, 7, 3),
                (Guid.Parse("18e5ba41-6710-4143-9808-704e88e94bd9"), "Катя", Transport.Car, 6, 9),
                // Добавьте здесь остальных курьеров, если необходимо
            };

            foreach (var data in couriersData)
            {
                var locationResult = Location.Create(data.X, data.Y);
                if (locationResult.IsFailure)
                {
                    // Обработка ошибки создания локации
                    continue;
                }

                var courierResult = Courier.Create(data.Id, data.Name, data.Transport);
                if (courierResult.IsSuccess)
                {
                    var courier = courierResult.Value;
                    _courierRepository.Add(courier);
                    Console.WriteLine("Принял курьера" + data.Name);
                    couriersNames.Add(courier.Name); 
                }
                else
                {
                    // Обработка ошибки создания курьера
                }
            }


            return Task.FromResult(couriersNames);
        }
    }
}
