﻿namespace DeliveryApp.Core.Application.UseCases.Queries.GetOrder
{
    public class Response
    {
        public Delivery Delivery { get; set; }

        public Response(Delivery delivery)
        {
            Delivery = delivery;
        }
    }


    public class Delivery
    {
        public Guid Id { get; set; }

        public Address Address { get; set; }
    
        public List<Item> Items { get; set; }

        protected Delivery()
        {

        }

        public Delivery(Guid id, Address address, List<Item> items): this()
        {
            Id = id;
            Address = address;
            Items = items;
        }
    }

    public class Address
    {
        private Address()
        {

        }

        public Address(string country, string city, string street, string house, string apartment):this()
        {
            Country = country;
            City = city;
            Street = street;
            House = house;
            Apartment = apartment;
        }
    
        /// <summary>
        ///     Страна
        /// </summary>
        public string Country { get; }

        /// <summary>
        ///     Город
        /// </summary>
        public string City { get; }

        /// <summary>
        ///     Улица
        /// </summary>
        public string Street { get; }

        /// <summary>
        ///     Дом
        /// </summary>
        public string House { get; }

        /// <summary>
        ///     Квартира
        /// </summary>
        public string Apartment { get; }
    }

    public class Item
    {
        /// <summary>
        /// Товар
        /// </summary>
        public Guid GoodId { get; }

        /// <summary>
        /// Количество
        /// </summary>
        public int Quantity { get; }

        /// <summary>
        /// Ctr
        /// </summary>
        private Item()
        {
        }

        /// <summary>
        /// Ctr
        /// </summary>
        public Item(Guid goodId, int quantity) : this()
        {
            GoodId = goodId;
            Quantity = quantity;
        }
    }
}