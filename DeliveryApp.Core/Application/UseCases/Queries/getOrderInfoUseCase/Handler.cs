﻿using Dapper;
using MediatR;
using Npgsql;

namespace DeliveryApp.Core.Application.UseCases.Queries.GetOrder
{
    public class Handler : IRequestHandler<Query, Response>
    {
        private readonly string _connectionString;

        public Handler()
        {
        
        }
   
        public Handler(string connectionString):this()
        {
            _connectionString = !string.IsNullOrWhiteSpace(connectionString) ? connectionString : throw new ArgumentNullException(nameof(connectionString));
        }

        public async Task<Response> Handle(Query message, CancellationToken cancellationToken)
        {
            using var connection = new NpgsqlConnection(_connectionString);
            connection.Open();

            var result = await connection.QueryAsync<dynamic>(
                @"SELECT *
                    FROM public.deliverys as b 
                    INNER JOIN public.items as i on b.id=i.delivery_id
                    WHERE b.id=@id;"
                , new { id = message.OrderId });

            if (result.AsList().Count == 0)
                return null;

            return new Response(MapDelivery(result));
        }
    
        private Delivery MapDelivery(dynamic result)
        {
            var address = new Address(result[0].address_country, result[0].address_city, result[0].address_street, result[0].address_house,
                result[0].address_apartment);
    
            var items = new List<Item>();
            foreach (dynamic dItem in result)
            {
                var item = new Item(dItem.good_id,dItem.quantity);
                items.Add(item);
            }
        
            var delivery = new Delivery(result[0].id, address,items);
            return delivery;
        }
    }
}