using CSharpFunctionalExtensions;
using Microsoft.VisualBasic;
using Primitives;

namespace DeliveryApp.Core.Domain.OrderAggregate
{
    public class OrderStatus : Entity<int>
    {
        public string Name { get; }

        public static readonly OrderStatus Created = new OrderStatus(1, nameof(Created));
        public static readonly OrderStatus Assigned = new OrderStatus(2, nameof(Assigned));
        public static readonly OrderStatus Completed = new OrderStatus(3, nameof(Completed));

        protected OrderStatus() { }

        private OrderStatus(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public static IEnumerable<OrderStatus> List() => new[] { Created, Assigned, Completed };

        public static Result<OrderStatus, Error> FromName(string name)
        {
            var  state = List()
            .SingleOrDefault( s => string.Equals(s.Name, name, StringComparison.CurrentCultureIgnoreCase));
            if (state == null) return Errors.InvalidStatus();
            return state;
        }

        public static Result<OrderStatus, Error> From (int id)
        {
            var state = List().SingleOrDefault(s => s.Id == id);
            if (state == null) return Errors.InvalidStatus();
            return state;
        }

        public static class Errors
        {
            public static Error InvalidStatus()
            {
                return new Error("order_status.invalid", $"Invalid order status. Valid statuses: {string.Join(", ", List().Select(s => s.Name))}.");
            }
        }
    }
}
