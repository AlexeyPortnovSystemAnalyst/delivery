using CSharpFunctionalExtensions;
using DeliveryApp.Core.Domain.CourierAggregate;
using DeliveryApp.Core.Domain.OrderAggregate.DomainEvents;
using DeliveryApp.Core.Domain.SharedKernel;
using Primitives;

namespace DeliveryApp.Core.Domain.OrderAggregate
{
    public class Order : Aggregate
    {
        // public Guid CourierId { get; private set; }
        public Guid? CourierId { get; protected set; }
        public Location Location { get; private set; }
        public Weight Weight { get; private set; }
        public OrderStatus Status { get; private set; }

        protected Order() { }

        protected Order(Guid id, Location location, Weight weight)
        {
            Id = id;
            Location = location;
            Weight = weight;
            Status = OrderStatus.Created;
            RaiseDomainEvent(new OrderCreatedDomainEvent(this));
        }

        public static Result<Order,Error > Create(Guid id, Location location, Weight weight)
        {
            if (id == Guid.Empty)
                return GeneralErrors.ValueIsRequired(nameof(id));

            // проверка зоны доставки - имитация, в ходе рефакторинга прокинуть делегата для проверки доступности зоны для доставки
            const int X_CITY_BORDER = 10;
            const int Y_CITY_BORDER = 10;

            if (id == Guid.Empty) return GeneralErrors.ValueIsRequired(nameof(id));
            if (location == null) return GeneralErrors.ValueIsRequired(nameof(location));
            if (weight == null) return GeneralErrors.ValueIsRequired(nameof(weight));

            // Проверка зоны доставки, хотя сам объект проверяет себя на диапазон, но пока оставим для имитации
            if (location.X > X_CITY_BORDER)
                return GeneralErrors.LocationIsOutRange("X", location.X, 1, X_CITY_BORDER);
            if (location.Y > Y_CITY_BORDER)
                return GeneralErrors.LocationIsOutRange("Y", location.Y, 1, Y_CITY_BORDER);
            // Дополнительные проверки, например, проверка веса
            if (weight.Value <= 0)
                return GeneralErrors.ValueIsInvalid(nameof(weight));

            return new Order(id, location, weight);
        }
        public Result<object, Error> AssignToCourier(Courier courier)
        {
            if (courier == null) return GeneralErrors.ValueIsRequired(nameof(courier));
            if (courier.Status == CourierStatus.Busy) return GeneralErrors.ValueIsRequired(nameof(courier)); // TODO
            
            CourierId = courier.Id;
            Status = OrderStatus.Assigned;
            courier.InWork();
            
            RaiseDomainEvent(new OrderAssignedDomainEvent(this));
            return new object();
        }


public Result<bool, Error > Complete()
{
    if (Status != OrderStatus.Assigned)
    {
        return OrderErrors.StatusIsWrong();
    }

    Status = OrderStatus.Completed;

    RaiseDomainEvent(new OrderAssignedDomainEvent(this));
    return true;
}

public static class OrderErrors
{
    public static Error StatusIsWrong()
    {
        return new Error("order.status_is_wrong", "Order status is not valid for this operation.");
    }
}

    }
}
