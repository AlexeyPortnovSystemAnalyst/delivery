using CSharpFunctionalExtensions;
using DeliveryApp.Core.Domain.SharedKernel;
using Primitives;

namespace DeliveryApp.Core.Domain.CourierAggregate
{
    public class Transport : Entity<int>
    {
        public string Name { get; }
        public int Speed { get; }
        public Weight Capacity { get; }

        public static readonly Transport Pedestrian = CreateTransport(1, "Pedestrian", 1, 1).Value;
        public static readonly Transport Bicycle = CreateTransport(2, "Bicycle", 2, 3).Value;
        public static readonly Transport Scooter = CreateTransport(3, "Scooter", 3, 4).Value;
        public static readonly Transport Car = CreateTransport(4, "Car", 4, 8).Value;

        protected Transport()
        {
        }

        private Transport(int id, string name, int speed, Weight capacity)
        {
            Id = id;
            Name = name;
            Speed = speed;
            Capacity = capacity;
        }

        private static Result<Transport, Error> CreateTransport(int id, string name, int speed, int capacityValue)
        {
            var capacityResult = Weight.Create(capacityValue);
            if (capacityResult.IsFailure)
            {
                return Errors.TransportIsWrong();
            }

            return new Transport(id, name, speed, capacityResult.Value);
        }

        public static IEnumerable<Transport> List() => new[] { Pedestrian, Bicycle, Scooter, Car };

        public bool CanCarry(Weight weight) => weight <= Capacity;

        /// <summary>
        /// Получить транспорт по идентификатору
        /// </summary>
        public static Result<Transport, Error> From(int id)
        {
            var transport = List().SingleOrDefault(t => t.Id == id);
            if (transport == null) return Errors.TransportIsWrong();
            return transport;
        }

/// <summary>
        ///     Ехать в вверх
        /// </summary>
        public Result<Location, Error> GoUp(Location location, out int leftoverSteps)
        {
            leftoverSteps = 0;
            var y = location.Y + Speed;
            if (y > Location.MaxLocation.Y)
            {
                leftoverSteps = y - Location.MaxLocation.Y;
                y = Location.MaxLocation.Y;
            }
            var result = Location.Create(location.X,y);
            if (result.IsFailure) return result.Error;
            return result.Value;
        }
        
        /// <summary>
        ///     Ехать в вниз
        /// </summary>
        public Result<Location, Error> GoDown(Location location, out int leftoverSteps)
        {
            leftoverSteps = 0;
            var y = location.Y - Speed;
            if (y < Location.MinLocation.Y)
            {
                leftoverSteps = y + Location.MinLocation.Y;
                y = Location.MinLocation.Y;
            }
            var result = Location.Create(location.X,y);
            if (result.IsFailure) return result.Error;
            return result.Value;
        }
        
        /// <summary>
        ///     Ехать в лево
        /// </summary>
        public Result<Location, Error> GoLeft(Location location, out int leftoverSteps)
        {
            leftoverSteps = 0;
            var x = location.X - Speed;
            if (x < Location.MinLocation.X)
            {
                leftoverSteps = x + Location.MinLocation.X;
                x = Location.MinLocation.X;
            }
            var result = Location.Create(x,location.Y);
            if (result.IsFailure) return result.Error;
            return result.Value;
        }
        
        /// <summary>
        ///     Ехать в право
        /// </summary>
        public Result<Location, Error> GoRight(Location location, out int leftoverSteps)
        {
            leftoverSteps = 0;
            var x = location.X + Speed;
            if (x > Location.MaxLocation.X)
            {
                leftoverSteps = x - Location.MaxLocation.X;
                x = Location.MaxLocation.X;
            }
            var result = Location.Create(x,location.Y);
            if (result.IsFailure) return result.Error;
            return result.Value;
        }

                /// <summary>
        /// Может ли данный транспорт перевезти данный вес
        /// </summary>
        /// <param name="weight">Вес</param>
        /// <returns>Результат</returns>
        public Result<bool, Error> CanAllocate(Weight weight)
        {
            if (weight > Capacity)
            {
                return false;
            }
            return true;
        }


        /// <summary>
        /// Ошибки, которые может возвращать сущность
        /// </summary>
        public static class Errors
        {
            public static Error TransportIsWrong()
            {
                return new Error($"{nameof(Transport).ToLowerInvariant()}.is.wrong",
                    $"Не верное значение. Допустимые значения: {nameof(Transport).ToLowerInvariant()}: {string.Join(", ", List().Select(t => t.Name))}");
            }
        }
    }
}
