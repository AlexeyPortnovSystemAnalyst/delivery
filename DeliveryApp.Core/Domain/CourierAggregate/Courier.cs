using CSharpFunctionalExtensions;
using DeliveryApp.Core.Domain.SharedKernel;
using Primitives;

namespace DeliveryApp.Core.Domain.CourierAggregate
{
    public class Courier : Aggregate
    {
        public string Name { get; private set; }
        public Transport Transport { get; private set; }
        public Location Location { get; private set; }
        public CourierStatus Status { get; private set; }

                /// <summary>
        /// Ctr
        /// </summary>
        protected Courier() {}


         protected Courier(string name, Transport transport):this()
        {
            Id = Guid.NewGuid();
            Name = name;
            Transport = transport;
            Location = Location.MinLocation;
            Status = CourierStatus.NotAvailable;
        }

        private Courier(Guid id, string name, Transport transport):this()
        {
            Id = id;
            Name = name;
            Transport = transport;
            Location = Location.MinLocation;
            Status = CourierStatus.NotAvailable;
        }


        public static Result<Courier, Error> Create(string name, Transport transport)
        {
            if (string.IsNullOrEmpty(name)) return GeneralErrors.ValueIsRequired(nameof(name));
            if (transport == null) return GeneralErrors.ValueIsRequired(nameof(transport));
            
            return new Courier(name,transport);
        }

        public static Result<Courier, Error> Create(Guid id,string name, Transport transport)
        {
            if (string.IsNullOrEmpty(name)) return GeneralErrors.ValueIsRequired(nameof(name));
            if (transport == null) return GeneralErrors.ValueIsRequired(nameof(transport));
            
            return new Courier(id, name,transport);
        }


        public Result<bool, Error> StartDay()
        {
            if (Status != CourierStatus.NotAvailable) return Errors.StatusIsWrong("Courier must be in NotAvailable status to start the day.");
            Status = CourierStatus.Ready;
            return true;
        }

        public Result<bool, Error> EndDay()
        {
            if (Status == CourierStatus.Busy) return Errors.StatusIsWrong("Courier cannot end the day while busy.");
            Status = CourierStatus.NotAvailable;
            return true;
        }

        public Result<bool, Error> CanCarry(Weight weight)
        {

            Console.WriteLine("Могу нести груз или нет?, я курьер - ", Name);
            // Проверяем, что экземпляр Transport не равен null
            if (Transport.Capacity == null)
            {
                Console.WriteLine("не могу, нет емкости");
                return GeneralErrors.ValueIsRequired(nameof(Transport));
            }

            // Используем метод CanCarry экземпляра Transport
            if (Transport.CanCarry(weight))
            {
                Console.WriteLine("Могу нести груз, я курьер - ", Name);
                return true;
            }
            else
            {
                return Errors.CannotCarryOrder("The courier's transport cannot carry the weight of the order.");
            }
        }
        public Result<bool, Error> AssignOrder()
        {
            if (Status != CourierStatus.Ready) return Errors.StatusIsWrong("Courier must be ready to take orders.");
            Status = CourierStatus.Busy;
            return true;
        }   

                /// <summary>
        /// Взять работу
        /// </summary>
        /// <returns>Результат</returns>
        public Result<object, Error> InWork()
        {
            if (Status == CourierStatus.NotAvailable) return Errors.TryAssignOrderWhenNotAvailable();
            Status = CourierStatus.Busy;
            return new object();
        }
        
        /// <summary>
        /// Завершить работу
        /// </summary>
        /// <returns>Результат</returns>
        public Result<object, Error> CompleteOrder()
        {
            Status = CourierStatus.Ready;
            return new object();
        }

        /// <summary>
        /// Рассчитать время до точки
        /// </summary>
        /// <param name="location">Конечное местоположение</param>
        /// <returns>Результат</returns>
        public Result<double, Error> CalculateTimeToLocation(Location location)
        {
            if (location == null) return GeneralErrors.ValueIsRequired(nameof(location));
            
            var distance = Location.DistanceTo(location).Value;
            var time = (double) distance / Transport.Speed;
            return time;
        }

       public Result<bool, Error> MoveTo(Location newLocation)
        {
            if (Status != CourierStatus.Ready && Status != CourierStatus.Busy)
                return Errors.StatusIsWrong("Courier must be ready or busy to move.");

    // Вычисляем разницу координат между текущим местоположением и новым
        var deltaX = newLocation.X - Location.X;
        var deltaY = newLocation.Y - Location.Y;

        // Определяем, в каком направлении нужно двигаться
        if (Math.Abs(deltaX) > Math.Abs(deltaY))
        {
            // Движение по оси X
            MoveHorizontally(deltaX);
        }
        else if (deltaY != 0) // Если разница по Y есть, то двигаемся по Y
        {
            // Движение по оси Y
            MoveVertically(deltaY);
        }

        return true;
    }

    private void MoveHorizontally(int deltaX)
    {
        var step = Math.Sign(deltaX) * Math.Min(Math.Abs(deltaX), Transport.Speed); // Шаг не больше скорости
        Location = Location.Create(Location.X + step, Location.Y).Value; // Обновляем локацию курьера
    }

    private void MoveVertically(int deltaY)
    {
        var step = Math.Sign(deltaY) * Math.Min(Math.Abs(deltaY), Transport.Speed); // Шаг не больше скорости
        Location = Location.Create(Location.X, Location.Y + step).Value; // Обновляем локацию курьера
    }

        // Добавление нового метода CalculateTravelTime
        public double CalculateTimeToPoint(Location destination)
        {
            var distance = Location.CalculateDistanceTo(destination);
            return (double)distance / Transport.Speed; // Время = расстояние / скорость
        }

        /// <summary>
        /// Изменить местоположение
        /// </summary>
        /// <param name="targetLocation">Геопозиция</param>
        /// <returns>Результат</returns>
        public Result<object, Error> Move(Location targetLocation)
        {
            if (targetLocation == null) return GeneralErrors.ValueIsRequired(nameof(targetLocation));

            for (;;)
            {
                int leftoverSteps;
                if (Location.X < targetLocation.X)
                {
                    // Если курьер правее цели, то двигаемся налево
                    var transportGoRightResult = Transport.GoRight(Location,out leftoverSteps);
                    if (transportGoRightResult.IsFailure) return transportGoRightResult.Error;

                    Location = transportGoRightResult.Value;
                    if (leftoverSteps==0)
                    {
                        break;
                    }
                }
                else if (Location.X > targetLocation.X)
                {
                    // Если курьер правее цели, то двигаемся налево
                    var transportGoRightResult = Transport.GoLeft(Location,out leftoverSteps);
                    if (transportGoRightResult.IsFailure) return transportGoRightResult.Error;
                    
                    Location = transportGoRightResult.Value;
                    if (leftoverSteps==0)
                    {
                        break;
                    }
                }
                else if (Location.Y < targetLocation.Y)
                {
                    // Если курьер ниже цели, то двигаемся вверх
                    var transportGoRightResult = Transport.GoUp(Location,out leftoverSteps);
                    if (transportGoRightResult.IsFailure) return transportGoRightResult.Error;
                    
                    Location = transportGoRightResult.Value;
                    if (leftoverSteps==0)
                    {
                        break;
                    }
                }
                else if (Location.Y > targetLocation.Y)
                {
                    // Если курьер выше цели, то двигаемся вниз
                    var transportGoRightResult = Transport.GoDown(Location,out leftoverSteps);
                    if (transportGoRightResult.IsFailure) return transportGoRightResult.Error;
                    
                    Location = transportGoRightResult.Value;
                    if (leftoverSteps==0)
                    {
                        break;
                    }
                }
            }
            return new object();
        }

    public static class Errors
    {
            public static Error StatusIsWrong(string message)
            {
                return new Error("courier.status_is_wrong", message);
            }

            public static Error CannotCarryOrder(string message)
            {
                return new Error("courier.cannot_carry_order", message);
            }

            public static Error TryStopWorkingWithIncompleteDelivery()
            {
                return new($"{nameof(Courier).ToLowerInvariant()}.try.stop.working.with.incomplete.delivery", "Нельзя прекратить работу, если есть незавершенная доставка");
            }
            
            public static Error TryStartWorkingWhenAlreadyStarted()
            {
                return new($"{nameof(Courier).ToLowerInvariant()}.try.start.working.when.already.started", "Нельзя начать работу, если ее уже начали ранее");
            }
            
            public static Error TryAssignOrderWhenNotAvailable()
            {
                return new($"{nameof(Courier).ToLowerInvariant()}.try.assign.order.when.not.available", "Нельзя взять заказ в работу, если курьер не начал рабочий день");
            }


    }

    }


}
