using CSharpFunctionalExtensions;
using Primitives;

namespace DeliveryApp.Core.Domain.CourierAggregate
{
    /// <summary>
    /// Статус курьера
    /// </summary>
    public class CourierStatus : Entity<int>
    {
        public static readonly CourierStatus NotAvailable = new CourierStatus(1, nameof(NotAvailable).ToLowerInvariant());
        public static readonly CourierStatus Ready = new CourierStatus(2, nameof(Ready).ToLowerInvariant());
        public static readonly CourierStatus Busy = new CourierStatus(3, nameof(Busy).ToLowerInvariant());

        /// <summary>
        /// Название статуса
        /// </summary>
        public virtual string Name { get; }

        protected CourierStatus()
        {
        }

        protected CourierStatus(int id, string name)
        {
            Id = id;
            Name = name;
        }

        /// <summary>
        /// Список всех статусов
        /// </summary>
        public static IEnumerable<CourierStatus> List()
        {
            yield return NotAvailable;
            yield return Ready;
            yield return Busy;
        }

        /// <summary>
        /// Получить статус по названию
        /// </summary>
        public static Result<CourierStatus, Error> FromName(string name)
        {
            var status = List().SingleOrDefault(s => string.Equals(s.Name, name, StringComparison.CurrentCultureIgnoreCase));
            if (status == null) return Errors.StatusIsWrong();
            return status;
        }

        /// <summary>
        /// Получить статус по идентификатору
        /// </summary>
        public static Result<CourierStatus, Error> From(int id)
        {
            var status = List().SingleOrDefault(s => s.Id == id);
            if (status == null) return Errors.StatusIsWrong();
            return status;
        }

        /// <summary>
        /// Ошибки, которые может возвращать сущность
        /// </summary>
        public static class Errors
        {
            public static Error StatusIsWrong()
            {
                return new Error($"{nameof(CourierStatus).ToLowerInvariant()}.is.wrong",
                    $"Не верное значение. Допустимые значения: {nameof(CourierStatus).ToLowerInvariant()}: {string.Join(",", List().Select(s => s.Name))}");
            }
        }
    }
}
