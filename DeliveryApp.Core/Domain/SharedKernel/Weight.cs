using CSharpFunctionalExtensions;
using Primitives;

namespace DeliveryApp.Core.Domain.SharedKernel
{
    public class Weight : ValueObject
    {
        public int Value { get; private set; }

        private Weight(int value)
        {
            Value = value;
        }

        // Изменяем метод Create для использования GeneralErrors
        public static Result<Weight, Error> Create(int value)
        {
            if (value <= 0) 
                return GeneralErrors.ValueIsRequired("weight");
            return new Weight(value);
        }

        public static bool operator <(Weight first, Weight second) => first.Value < second.Value;
        public static bool operator <=(Weight first, Weight second) => first.Value <= second.Value;
        public static bool operator >(Weight first, Weight second) => first.Value > second.Value;
        public static bool operator >=(Weight first, Weight second) => first.Value >= second.Value;
        public static bool operator ==(Weight first, Weight second) => first.Equals(second);
        public static bool operator !=(Weight first, Weight second) => !(first == second);

        

        protected override IEnumerable<IComparable> GetEqualityComponents()
        {
            yield return Value;
        }

                public override bool Equals(object obj)
        {
            if (obj is Weight other)
                return Value == other.Value;
            return false;
        }

        public override int GetHashCode() => Value.GetHashCode();
    }
}
