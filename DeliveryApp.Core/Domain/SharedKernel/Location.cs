using CSharpFunctionalExtensions;
using Primitives;

namespace DeliveryApp.Core.Domain.SharedKernel
{
    public class    Location : ValueObject
    {
        public const int MinRange = 1;
        public const int MaxRange = 10;

        public int X { get; private set; }
        public int Y { get; private set; }

                /// <summary>
        /// Макимально возможная координата
        /// </summary>
        public static readonly Location MinLocation = new Location(1,1);
        
        /// <summary>
        /// Макимально возможная координата
        /// </summary>
        public static readonly Location MaxLocation = new Location(10,10);

        private Location(int x, int y)
        {
            X = x;
            Y = y;
        }

        public static Result<Location, Error> Create(int x, int y)
        {

            // не совсем хорошо что локация отвечает за диапазон, это должно быть в бизнес логике.
            if (x < MinRange || x > MaxRange)
                return GeneralErrors.LocationIsOutRange("X", x, MinRange, MaxRange);
            if (y < MinRange || y > MaxRange)
                return GeneralErrors.LocationIsOutRange("Y", y, MinRange, MaxRange);
            
            return new Location(x, y);
        }

        /// <summary>
        /// Рассчитать дистанцию
        /// </summary>
        /// <param name="targetLocation">Конечная координата</param>
        /// <returns>Результат</returns>
        public Result<int, Error> DistanceTo(Location targetLocation)
        {
            var diff = targetLocation - this;
            var distance = diff.X + diff.Y; //количество ходов по клеткам
            return distance;
        }

                /// <summary>
        /// Сложить 2 координаты
        /// </summary>
        /// <param name="first">Первая</param>
        /// <param name="second">Вторая</param>
        /// <returns>Результат</returns>
        public static Location operator +(Location first,Location second)
        {
            Location result = new Location((first.X + second.X),first.Y+ second.Y);
            return result;
        }
        
        /// <summary>
        /// Вычесть 2 координаты
        /// </summary>
        /// <param name="first">Первая</param>
        /// <param name="second">Вторая</param>
        /// <returns>Результат</returns>
        public static Location operator -(Location first,Location second)
        {
            var result = new Location(Math.Abs(first.X - second.X),Math.Abs(first.Y - second.Y));
            return result;
        }


        public int CalculateDistanceTo(Location other)
        {
            return Math.Abs(X - other.X) + Math.Abs(Y - other.Y);
        }

        protected override IEnumerable<IComparable> GetEqualityComponents()
        {
            yield return X;
            yield return Y;
        }
    }
}
