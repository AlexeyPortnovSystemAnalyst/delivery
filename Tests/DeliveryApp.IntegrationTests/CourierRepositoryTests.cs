using DeliveryApp.Core.Domain.CourierAggregate;
using DeliveryApp.Core.Ports;
using DeliveryApp.Infrastructure;
using DeliveryApp.Infrastructure.Adapters.Postgres;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Testcontainers.PostgreSql;
using Xunit;

namespace DeliveryApp.IntegrationTests;

public class CourierRepositoryShould : IAsyncLifetime
{
    private ApplicationDbContext _context;
    private readonly PostgreSqlContainer _postgreSqlContainer = new PostgreSqlBuilder()
        .WithImage("postgres:14.7")
        .WithDatabase("delivery")
        .WithUsername("username")
        .WithPassword("secret")
        .WithCleanUp(true)
        .Build();

    public async Task InitializeAsync()
    {
        await _postgreSqlContainer.StartAsync();
        
        var contextOptions = new DbContextOptionsBuilder<ApplicationDbContext>().UseNpgsql(_postgreSqlContainer.GetConnectionString(),
                npgsqlOptionsAction: sqlOptions =>
                {
                    sqlOptions.EnableRetryOnFailure(maxRetryCount: 15, maxRetryDelay: TimeSpan.FromSeconds(30), errorCodesToAdd: null);                        
                })
            .Options;
        _context = new ApplicationDbContext(contextOptions);
        _context.Database.Migrate();
    }

    public async Task DisposeAsync()
    {
        await _postgreSqlContainer.DisposeAsync().AsTask();
    }

    [Fact]
    public async void CanAddCourier()
    {
        //Arrange
        var courierCreateResult = Courier.Create("Иван", Transport.Pedestrian);
        courierCreateResult.IsSuccess.Should().BeTrue();
        var courier = courierCreateResult.Value;
        
        //Act
        var courierRepository = new CourierRepository(_context);
        courierRepository.Add(courier);
        await courierRepository.UnitOfWork.SaveEntitiesAsync();
        
        //Assert
        var courierFromDb= await courierRepository.GetAsync(courier.Id);
        courier.Should().BeEquivalentTo(courierFromDb);

    }

        [Fact]
    public async void CanUpdateCourier()
    {
        //Arrange
        var courierCreateResult = Courier.Create("Иван", Transport.Pedestrian);
        courierCreateResult.IsSuccess.Should().BeTrue();
        var courier = courierCreateResult.Value;
        
        var courierRepository = new CourierRepository(_context);
        courierRepository.Add(courier);
        await courierRepository.UnitOfWork.SaveEntitiesAsync();
        
        //Act
        var courierStartWorkResult = courier.StartDay();
        courierStartWorkResult.IsSuccess.Should().BeTrue();
        courierRepository.Update(courier);
        await courierRepository.UnitOfWork.SaveEntitiesAsync();
        
        //Assert
        var courierFromDb= await courierRepository.GetAsync(courier.Id);
        courier.Should().BeEquivalentTo(courierFromDb);
        courierFromDb.Status.Should().Be(CourierStatus.Ready);
    }


    [Fact]
    public async void CanGetById()
    {
        //Arrange
        var courierCreateResult = Courier.Create("Иван", Transport.Pedestrian);
        courierCreateResult.IsSuccess.Should().BeTrue();
        var courier = courierCreateResult.Value;
        
        //Act
        var courierRepository = new CourierRepository(_context);
        courierRepository.Add(courier);
        await courierRepository.UnitOfWork.SaveEntitiesAsync();
        
        //Assert
        var courierFromDb= await courierRepository.GetAsync(courier.Id);
        courier.Should().BeEquivalentTo(courierFromDb);
    }
    
    [Fact]
    public async void CanGetAllActive()
    {
        //Arrange
        var courier1CreateResult = Courier.Create("Иван", Transport.Pedestrian);
        courier1CreateResult.IsSuccess.Should().BeTrue();
        var courier1 = courier1CreateResult.Value;
        courier1.EndDay();
        
        var courier2CreateResult = Courier.Create("Борис", Transport.Pedestrian);
        courier2CreateResult.IsSuccess.Should().BeTrue();
        var courier2 = courier2CreateResult.Value;
        courier2.StartDay();
        
        var courierRepository = new CourierRepository(_context);
        courierRepository.Add(courier1);
        courierRepository.Add(courier2);
        await courierRepository.UnitOfWork.SaveEntitiesAsync();
        
        //Act
        var activeCouriersFromDb= courierRepository.GetAllActive();
        
        //Assert
        activeCouriersFromDb.Should().NotBeEmpty();
        activeCouriersFromDb.Count().Should().Be(1);
        activeCouriersFromDb.First().Should().BeEquivalentTo(courier2);
    }



}
