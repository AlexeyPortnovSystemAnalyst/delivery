using Xunit;
using DeliveryApp.Core.Domain.CourierAggregate;
using DeliveryApp.Core.Domain.OrderAggregate;
using DeliveryApp.Core.Domain.SharedKernel;
using CSharpFunctionalExtensions;

public class OrderTests
{
    [Fact]
    public void Create_ValidParameters_ShouldSucceed()
    {
        // Arrange
        var id = Guid.NewGuid();
        var locationResult = Location.Create(5, 6);
        Assert.True(locationResult.IsSuccess, "Location creation must succeed for the test to proceed.");
        var location = locationResult.Value;
        var weightResult = Weight.Create(10); 
        Assert.True(weightResult.IsSuccess, "Weight creation must succeed for the test to proceed."); 
        var weight = weightResult.Value;

        // Act
        var result = Order.Create(id, location, weight);

        // Assert
        Assert.True(result.IsSuccess);
    }

    [Fact]
    public void AssignToCourier_WhenCreated_ShouldSucceed()
    {
        // Arrange
        var locationResult = Location.Create(5, 6);
        Assert.True(locationResult.IsSuccess, "Location creation must succeed for the test to proceed.");
        var location = locationResult.Value;
        var weightResult = Weight.Create(10); 
        Assert.True(weightResult.IsSuccess, "Weight creation must succeed for the test to proceed."); 
        var weight = weightResult.Value;
        var orderResult = Order.Create(Guid.NewGuid(), location, weight);
        Assert.True(orderResult.IsSuccess, "Order creation must succeed for the test to proceed.");
        var order = orderResult.Value;
        var courierId = Guid.NewGuid();

        // Act
        var result = order.AssignToCourier(courierId);

        // Assert
        Assert.True(result.IsSuccess);
    }

}
