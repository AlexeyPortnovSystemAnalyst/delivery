using Xunit;
using DeliveryApp.Core.Domain.CourierAggregate;
using DeliveryApp.Core.Domain.SharedKernel;
using CSharpFunctionalExtensions;

public class TransportTests
{
    [Theory]
    [InlineData(1, true)] // Предполагаем, что велосипед может перевозить вес в 1 единицу
    [InlineData(5, false)] // Предполагаем, что велосипед не может перевозить вес в 5 единиц, если его грузоподъемность меньше
    public void CanCarry_GivenWeight_ShouldReturnExpectedResult(int weightValue, bool expectedResult)
    {
        // Arrange
        var transport = Transport.Bicycle;
        var weightResult = Weight.Create(weightValue);
        Assert.True(weightResult.IsSuccess, "Failed to create weight object for the test."); // Убедимся, что создание объекта Weight прошло успешно
        var weight = weightResult.Value;

        // Act
        var result = transport.CanCarry(weight);

        // Assert
        Assert.Equal(expectedResult, result);
    }
}
