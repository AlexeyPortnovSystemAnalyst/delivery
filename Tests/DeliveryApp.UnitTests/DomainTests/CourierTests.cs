using Xunit;
using DeliveryApp.Core.Domain.CourierAggregate;
using DeliveryApp.Core.Domain.OrderAggregate;
using CSharpFunctionalExtensions;

public class CourierTests
{
    [Fact]
    public void Create_ValidParameters_ShouldSucceed()
    {
        // Arrange
        var name = "Test Courier";
        var transport = Transport.Bicycle; // Предполагаем, что Transport уже тестируется отдельно

        // Act
        var result = Courier.Create(name, transport);

        // Assert
        Assert.True(result.IsSuccess);
    }

    [Fact]
    public void StartDay_WhenNotAvailable_ShouldMakeReady()
    {
        // Arrange
        var courier = Courier.Create("Test Courier", Transport.Bicycle).Value;

        // Act
        courier.StartDay();

        // Assert
        Assert.Equal(CourierStatus.Ready, courier.Status);
    }

    [Fact]
    public void EndDay_WhenReady_ShouldMakeNotAvailable()
    {
        // Arrange
        var courier = Courier.Create("Test Courier", Transport.Bicycle).Value;
        courier.StartDay();

        // Act
        courier.EndDay();

        // Assert
        Assert.Equal(CourierStatus.NotAvailable, courier.Status);
    }
}
