using Xunit;
using DeliveryApp.Core.Domain.SharedKernel;
using FluentAssertions;
using Primitives; 

namespace DeliveryApp.Domain.SharedKernel;

public class LocationShould
{
    [Theory]
    [InlineData(1, 1)]
    [InlineData(10, 10)]
    public void BeCorrectWhenParamsAreCorrectOnCreated(int x, int y)
    {
        //Act
        var location = Location.Create(x, y);

        //Assert
        location.IsSuccess.Should().BeTrue();
        location.Value.X.Should().Be(x);
        location.Value.Y.Should().Be(y);
    }

    [Theory]
    [InlineData(0, 5)]
    [InlineData(11, 5)]
    [InlineData(5, 0)]
    [InlineData(5, 11)]
    public void ReturnErrorWhenParamsAreIncorrectOnCreated(int x, int y)
    {
        //Act
        var location = Location.Create(x, y);

        //Assert
        location.IsSuccess.Should().BeFalse();
        location.Error.Should().NotBeNull();
    }

    [Fact]
    public void CalculateDistanceCorrectly()
    {
        //Arrange
        var location1 = Location.Create(1, 1).Value;
        var location2 = Location.Create(3, 4).Value;

        //Act
        var distance = location1.CalculateDistanceTo(location2);

        //Assert
        distance.Should().Be(5);
    }

    [Fact]
    public void BeEqualWhenAllPropertiesAreEqual()
    {
        //Arrange
        var firstLocation = Location.Create(5, 5).Value;
        var secondLocation = Location.Create(5, 5).Value;

        //Act & Assert
        firstLocation.Should().BeEquivalentTo(secondLocation);
    }
}
