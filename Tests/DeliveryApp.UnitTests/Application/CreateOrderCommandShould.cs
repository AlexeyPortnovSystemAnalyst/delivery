using System;
using System.Threading;
using System.Threading.Tasks;
using DeliveryApp.Core.Application.UseCases.Commands.CreateOrder;
using DeliveryApp.Core.Domain.OrderAggregate;
using DeliveryApp.Core.Ports;
using MediatR;
using NSubstitute;
using Xunit;
using FluentAssertions;

namespace DeliveryApp.UnitTests.Application;

public class CreateOrderCommandShould
{
    private readonly IOrderRepository _orderRepositoryMock;

    public CreateOrderCommandShould()
    {
        _orderRepositoryMock = Substitute.For<IOrderRepository>();
    }
    
    [Fact]
    public async Task ReturnTrueWhenOrderIsSuccessfullyCreated()
    {
        // Arrange
        var basketId = Guid.NewGuid();
        var street = "Test Street";
        var weight = 5;
        var command = new Command(basketId, street, weight);
        var handler = new Handler(_orderRepositoryMock);

        _orderRepositoryMock.UnitOfWork.SaveEntitiesAsync(Arg.Any<CancellationToken>())
            .Returns(true);
        
        // Act
        var result = await handler.Handle(command, new CancellationToken());

        // Assert
        result.Should().BeTrue();
        _orderRepositoryMock.Received(1).Add(Arg.Any<Order>());
        await _orderRepositoryMock.UnitOfWork.Received(1).SaveEntitiesAsync(Arg.Any<CancellationToken>());
    }

    [Fact]
    public async Task ReturnFalseWhenWeightIsInvalid()
    {
        // Arrange
        var basketId = Guid.NewGuid();
        var street = "Test Street";
        var invalidWeight = 0; // Указываем недопустимый вес
        var command = new Command(basketId, street, invalidWeight);
        var handler = new Handler(_orderRepositoryMock);

        // Act
        var result = await handler.Handle(command, new CancellationToken());

        // Assert
        result.Should().BeFalse();
        _orderRepositoryMock.DidNotReceive().Add(Arg.Any<Order>());
        await _orderRepositoryMock.UnitOfWork.DidNotReceive().SaveEntitiesAsync(Arg.Any<CancellationToken>());
    }
}
